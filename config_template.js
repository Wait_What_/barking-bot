module.exports = {
    tokens: {
        discord: '', // Do not share the token with anyone
    },
    defaultPrefix: 'd.',
    owners: [ // User IDs of owners of the bot
        ''
    ],
    shards: 'auto', // 'auto' or number, up to 2500 guilds per shard
    name: 'BarkingBot-clone',
    embedColor: '#ff0000', // Default color for embeds
    activity: {
        text: 'BOTservers servers | BOTprefixhelp',
        /* BOTservers - server count
           BOTusers - user count
           BOTprefix - default prefix */
        type: 'PLAYING' 
        /* PLAYING
           STREAMING
           LISTENING
           WATCHING */

    },  
    logCommands: {
        enabled: false, // Creates a mess in the console
        ignoreBotOwners: true
    },
    currency: {
        name: 'Barkcoin-clone',
        emote: '',
        icon: ''
    },
    dbDefaults: {
        user: {
            balance: 0,
            blacklist: false
        },
        guild: {
            prefix: undefined,
            snipe: {},
            warnings: {},
            currency: {
                channels: [],
                frequency: 30
            },
            roleshop: [],
            greetings: {
                embed: false,
                dm: false,
                join: {
                    channel: undefined,
                    text: ''
                },
                leave: {
                    channel: undefined,
                    text: ''
                }
            }
        }
    }
}