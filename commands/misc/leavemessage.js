const { db, functions } = require('../../bot')

exports.run = (m, a) => {
    functions.ensure.guild(m.guild.id)
    const message = a.join(' ')

    if (!message) {
        functions.respond(m, 'Disabled leave messages')

        db.guild.set(m.guild.id, '', 'greetings.leave.text')
    } else {
        const embed = functions.embed()
            .setAuthor('Updated leave messages', m.guild.iconURL)
            .setDescription(message)
            .addField('You can also use:', '`{{membercount}}`, `{{membertag}}`, `{{membername}}` and ' +
                '`{{servername}}`\nYou can use `greetoptions` to change the style of the message ' +
                'or to have it sent in DMs')

        db.guild.set(m.guild.id, {
            channel: m.channel.id,
            text: message
        }, 'greetings.leave')
            
        m.channel.send({embed})
    }
}

exports.meta = {
    names: ['leavemessage', 'goodbye'],
    permissions: ['MANAGE_MESSAGES'],
    help: {
        description: 'Change the leave message in this channel',
        usage: '[text]',
        category: 'misc'
    }
}