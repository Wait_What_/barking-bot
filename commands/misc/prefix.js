const { bot, db, functions, config } = require('../../bot')

exports.run = (m, a) => {
    const prefix = a.join(' ')

    functions.ensure.guild(m.guild.id)

    if (prefix && prefix != config.defaultPrefix) {
        db.guild.set(m.guild.id, prefix, 'prefix')

        functions.respond(m, `Set the prefix to \`${prefix}\``)
    } else {
        db.guild.set(m.guild.id, undefined, 'prefix')

        functions.respond(m, `Reset the prefix to \`${config.defaultPrefix}\``)
    }
}

exports.meta = {
    names: ['prefix', 'setprefix'],
    permissions: ['MANAGE_MESSAGES'],
    help: {
        description: 'Change the prefix in this server',
        usage: '[new-prefix]',
        category: 'misc'
    }
}