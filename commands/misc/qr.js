const { functions } = require('../../bot')

exports.run = (m, a) => {
    const text = a.join(' ')
    if (!text || text.length > 256) return functions.respond(m, 'Provide a valid request (up to 256 characters)')
    
    let size = 128 + 8 * text.length > 1000 ? 1000 : 128 + 8 * text.length

    const embed = functions.embed()
        .setImage('https://api.qrserver.com/v1/create-qr-code/?size=' +
            `${size}x${size}&data=${text.replace(/ /gi, '%20').replace(/\//gi, '%2F')}`)
        
    m.channel.send({embed})
}

exports.meta = {
    names: ['generateqr', 'qr', 'qrcode', 'makeqr', 'genqr', 'createqr'],
    permissions: [],
    help: {
        description: 'Generate a QR code',
        usage: 'text',
        category: 'misc'
    }
}