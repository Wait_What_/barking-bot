const { functions } = require('../../bot')

exports.run = (m, a) => {
    const parseDate = date => new Date(date).toISOString().split('.')[0].replace('T', ' ')

    const embed = functions.embed()
        .setAuthor(m.guild.name, m.guild.iconURL)
        .addField('Stats', `${m.guild.memberCount} members: ` +
            `${m.guild.members.filter(member => !member.user.bot).size} humans, ` +
            `${m.guild.members.filter(member => member.user.bot).size} bots\n` + 
            `${m.guild.channels.size} channels\n${m.guild.roles.size} roles`)
        .addField('Owner', m.guild.owner)
        .setThumbnail(m.guild.iconURL)
        .setFooter(`ID: ${m.guild.id} | Created at: ${parseDate(m.guild.createdAt)}`)

    m.channel.send({embed})
}

exports.meta = {
    names: ['serverinfo', 'guildinfo'],
    permissions: [],
    help: {
        description: 'See info about this server',
        usage: '',
        category: 'misc'
    }
}