const { functions } = require('../../bot')
const emotes = ['1⃣', '2⃣', '3⃣', '4⃣', '5⃣', '6⃣', '7⃣', '8⃣', '9⃣']

exports.run = (m, a) => {
    const pollArr = a.join(' ').split('|')
    const question = pollArr[0]
    const choices = pollArr.slice(1)

    if (!question || !question.length || question.length < 1 || question.length > 256)
        return functions.respond(m, 'Provide a valid question (1 to 256 characters)')

    const embed = functions.embed()
        .setTitle(question)
        .setFooter(`Poll by ${m.author.tag}`, m.author.displayAvatarURL)

    if (choices.length > 0) {
        if (choices.length > emotes.length)
            return functions.respond(m, 'Too many choices (up to 9 choices)')

        if (choices.find(choice => choice == undefined))
            return functions.respond(m, 'Some choices are invalid')
        
        let i = 0
        const description = choices
            .map(choice => `${emotes[i++]} ${choice || ''}`)

        if (description.length > 2048)
            return functions.respond(m, 'The choices are too long (up to 2048 characters)')

        embed.setDescription(description)

        m.channel.send({embed})
            .then(async newM => {
                let y = 0
                while (y < choices.length) {
                    await newM.react(emotes[y++])
                }
            })
    } else {
        m.channel.send({embed})
            .then(async newM => {
                await newM.react('✅')
                newM.react('❎')
            })
    }
}

exports.meta = {
    names: ['poll', 'p'],
    permissions: [],
    help: {
        description: 'Create a poll',
        usage: 'question [| option-a | option-b | ...]',
        category: 'misc'
    }
}