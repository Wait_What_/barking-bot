const { db, functions } = require('../../bot')

exports.run = (m, a) => {
    functions.ensure.guild(m.guild.id)
    const message = a.join(' ')

    if (!message) {
        functions.respond(m, 'Disabled join messages')

        db.guild.set(m.guild.id, '', 'greetings.join.text')
    } else {
        const embed = functions.embed()
            .setAuthor('Updated join messages', m.guild.iconURL)
            .setDescription(message)
            .addField('You can also use:', '`{{membercount}}`, `{{membertag}}`, `{{membername}}`, ' +
                '`{{servername}}` and `{{memberping}}`\nYou can use `greetoptions` to change the style of the message ' +
                'or have it sent in DMs')

        db.guild.set(m.guild.id, {
            channel: m.channel.id,
            text: message
        }, 'greetings.join')

        m.channel.send({embed})
    }
}

exports.meta = {
    names: ['joinmessage', 'greet', 'greeting'],
    permissions: ['MANAGE_MESSAGES'],
    help: {
        description: 'Change the leave message in this channel',
        usage: '[text]',
        category: 'misc'
    }
}