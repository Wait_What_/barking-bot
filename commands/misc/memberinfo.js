const { functions } = require('../../bot')

exports.run = (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0]) || m.member
    const parseDate = date => new Date(date).toISOString().split('.')[0].replace('T', ' ')

    const embed = functions.embed()
        .setAuthor(`${member.user.tag}`, member.user.displayAvatarURL, member.user.displayAvatarURL)
        .addField('Nickname', member.displayName, true)
        .addField('Roles', member.roles.size, true)
        .addField('Status', member.user.presence.status, true)
        .addField('Registration date', parseDate(member.user.createdAt), true)
        .addField(`Join date`, parseDate(m.guild.member(member).joinedAt), true)
        .setFooter(`ID: ${member.user.id} | Click the tag for their avatar`)

    m.channel.send({embed})
}

exports.meta = {
    names: ['memberinfo', 'whois', 'userinfo'],
    permissions: [],
    help: {
        description: 'See info about a member',
        usage: '',
        category: 'misc'
    }
}