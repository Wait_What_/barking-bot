const { bot, functions } = require('../../bot')

exports.run = (m, a) => {
    const embed = functions.embed()
        .setAuthor('EULA', bot.user.displayAvatarURL)
        .setDescription(
            'By using this bot you agree that some data may be collected:\n\n' +
            
            '- Your user ID will be saved if you use some of the commands such as `balance`\n' +
            '- The last deleted message in a channel for the `undelete` command\n' +
            '- Your user ID will be saved if you vote for the bot\n' +
            '- Your and the moderator\'s user IDs if the `warn` command is used\n' +
            '- ...\n\n' +

            'This will never be shared and it\'s required for the bot to function properly\n\n' +

            'Data that is NOT collected:\n\n' +

            '- Your username and tag\n' +
            '- Your regular messages\n'
        )

    m.channel.send({embed})
}

exports.meta = {
    names: ['eula'],
    permissions: [],
    help: {
        description: 'See the bot\'s EULA',
        usage: '',
        category: 'misc'
    }
}