const { bot, functions } = require('../../bot')
const { help } = require('../../modules/core/commandLoader')

exports.run = (m, a) => {
    const prefix = functions.getPrefix(m.guild.id)

    const embed = functions.embed()
        .setAuthor('Help', bot.user.displayAvatarURL, 'https://waitwhat.xyz/bb/cmd')
        .setFooter('https://waitwhat.xyz/bb/cmd')

    const sendList = () => {
        const list = Array.from(help.keys())
            .map(category => `\`${prefix}help ${category}\``)

        embed.addField('To see help, use:', `\`${prefix}help all\`\n${list.join('\n')}`)
        m.channel.send({embed})
    }

    if (a.length < 1 || !a[0] || typeof a[0] != 'string') return sendList()
    
    if (Array.from(help.keys()).includes(a[0].toLowerCase())) {
        const list = help.get(a[0].toLowerCase())
            .map(item => `\`${prefix}${item.names[0]}\` ${item.description}${item.usage != '' ? ` \`${prefix}${item.names[0]} ${item.usage}\`` : ''}`)
        
        embed.addField(a[0][0].toUpperCase() + a[0].slice(1).toLowerCase(), list.join('\n'))

        m.channel.send({embed})
        return
    } else if (a[0].toLowerCase() == 'all') {
        Array.from(help.keys()).forEach(key => {
            const list = help.get(key)
                .map(item => `\`${prefix}${item.names[0]}\` ${item.description}${item.usage != '' ? ` \`${prefix}${item.names[0]} ${item.usage}\`` : ''}`)
            
            embed.addField(key[0].toUpperCase() + key.slice(1).toLowerCase(), list.join('\n'))
        })

        m.channel.send({embed})
        return
    } else sendList()
}

exports.meta = {
    names: ['help'],
    permissions: [],
    help: {
        description: 'See the list of all commands',
        usage: '',
        category: 'misc'
    }
}