const { functions } = require('../../bot')

exports.run = (m, a) => {
    let color = Math.floor(Math.random() * 16777215).toString(16).slice(-6)
    if (a[0]) {
        const toTest = a[0].replace(/#| /g, '')
        if (!/^#[0-9A-F]{6}$/i.test(`#${toTest}`))
            return functions.respond(m, 'Provide a valid color or don\'t provide a color')
        else color = toTest
    }
    
    const embed = functions.embed(color)
        .setDescription(`#${color}`)
        .setImage(`https://placehold.it/100x100.png/${color}/${color}`)
        
    m.channel.send({embed})
}

exports.meta = {
    names: ['color', 'colour'],
    permissions: [],
    help: {
        description: 'Get a (random) color',
        usage: '[hex-color]',
        category: 'misc'
    }
}