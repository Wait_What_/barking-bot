const { bot, functions, config } = require('../../bot')

exports.run = (m, a) => {
    // Please do not remove the credit, thanks
    const embed = functions.embed()
        .setAuthor('Info', bot.user.displayAvatarURL, 'https://barking-dog.xyz/bb/git')
        .addField(config.name, 'This is a multifunctional, open source discord bot made by BarkingDog#4975')
        .addField('Stats', `Running in ${bot.guilds.size} servers with the default prefix ` +
            `\`${config.defaultPrefix}\`\nThe owner${config.owners.length > 1 ? 's are: ' : ' is '}` + 
            config.owners.map(id => bot.users.get(id).tag).join(', '))
        .addField('Useful links', 'Invite: https://waitwhat.xyz/bb/inv\nReport bugs: https://waitwhat.xyz/bb/bug\n' +
            'Repository: https://waitwhat.xyz/bb/git\nSupport server: https://discord.gg/N8Fqcuk')
            
    m.channel.send({embed})
}

exports.meta = {
    names: ['info', 'stats', 'information', 'invite', 'bugs', 'support'],
    permissions: [],
    help: {
        description: 'See info about the bot',
        usage: '',
        category: 'misc'
    }
}