const { functions } = require('../../bot')

exports.run = (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0]) || m.member
    if (!member) return functions.respond(m, 'Mention a valid member')

    const embed = functions.embed()
        .setAuthor(member.user.tag, member.user.displayAvatarURL, member.user.displayAvatarURL)
        .setImage(member.user.displayAvatarURL)

    m.channel.send({embed})
}

exports.meta = {
    names: ['avatar', 'pfp', 'picture', 'profile', 'av'],
    permissions: [],
    help: {
        description: 'See a member\'s avatar',
        usage: '[@member]',
        category: 'misc'
    }
}