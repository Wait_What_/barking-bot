const { bot, db, functions } = require('../../bot')

exports.run = (m, a) => {
    const prefix = functions.getPrefix(m.guild.id)
    const greetings = db.guild.get(m.guild.id, 'greetings')

    if (!a[0] || isNaN(a[0]) || Number(a[0]) > 4 || Number(a[0]) < 1) {
        const embed = functions.embed()
            .setAuthor('Greeting options', bot.user.displayAvatarURL)
            .setDescription(`Usage: \`${prefix}greetoptions 1-4\``)
            .addField('Possible choices',
                '1. Embeds **on**, DMs **on**\n2. Embeds **on**, DMs **off**\n' +
                '3. Embeds **off**, DMs **on**\n4. Embeds **off**, DMs **off**')
            .addField('Current settings', `Embeds **${greetings.embed ? 'on' : 'off'}**, DMs **${greetings.dm ? 'on' : 'off'}**`)
                

        m.channel.send({embed})
    } else {
        if (a[0] == 1) {
            db.guild.set(m.guild.id, true, 'greetings.embed')
            db.guild.set(m.guild.id, true, 'greetings.dm')
        } else if (a[0] == 2) {
            db.guild.set(m.guild.id, true, 'greetings.embed')
            db.guild.set(m.guild.id, false, 'greetings.dm')
        } else if (a[0] == 3) {
            db.guild.set(m.guild.id, false, 'greetings.embed')
            db.guild.set(m.guild.id, true, 'greetings.dm')
        } else if (a[0] == 4) {
            db.guild.set(m.guild.id, false, 'greetings.embed')
            db.guild.set(m.guild.id, false, 'greetings.dm')
        }

        functions.respond(m, 'Updated greeting options')
    }
}

exports.meta = {
    names: ['greetoptions', 'joinoptions', 'leaveoptions'],
    permissions: ['MANAGE_MESSAGES'],
    help: {
        description: 'Change greet options',
        usage: '[embed] [dm]',
        category: 'misc'
    }
}