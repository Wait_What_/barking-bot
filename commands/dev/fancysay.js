const { functions } = require('../../bot')

exports.run = (m, a) => {
    if (a.length < 1) return

    functions.respond(m, a.join(' '))
}

exports.meta = {
    names: ['fancysay', 'fancytalk'],
    permissions: ['BOT_OWNER'],
    help: {
        description: 'Make the bot say something in an embed',
        usage: 'text',
        category: ''
    }
}