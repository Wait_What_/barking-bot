exports.run = (m, a) => {
    if (a.length < 1) return

    m.channel.send(a.join(' '))
}

exports.meta = {
    names: ['say', 'talk'],
    permissions: ['BOT_OWNER'],
    help: {
        description: 'Make the bot say something',
        usage: 'text',
        category: ''
    }
}