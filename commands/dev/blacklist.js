const { bot, functions, db } = require('../../bot')

exports.run = (m, a) => {
    const user = m.mentions.users.first() || bot.users.get(a[0])
    if (!user || user.id == m.author.id) return functions.respond(m, 'Provide a valid user')

    functions.ensure.user(user.id)
    const current = db.user.get(user.id, 'blacklist')

    functions.respond(m, `${current ? 'Unb' : 'B'}lacklisted **${user.tag}**`)

    db.user.set(user.id, !current, 'blacklist')
}

exports.meta = {
    names: ['blacklist'],
    permissions: ['BOT_OWNER'],
    help: {
        description: 'Blacklist a user',
        usage: '',
        category: ''
    }
}