const { db, functions } = require('../../bot')

exports.run = (m, a) => {
    if (!a[0] || isNaN(a[0]) || Number(a[0]) < 15 || Number(a[0]) > 150)
        return functions.respond(m, 'Provide a valid number from 15 to 150')
    
    const frequency = Math.round(Number(a[0]))
    functions.ensure.guild(m.guild.id)
    db.guild.set(m.guild.id, frequency, 'currency.frequency')

    functions.respond(m, `There's now a **20%** chance to generate **1-50**  ${functions.emote()}  after reaching ` + 
        `**${frequency}** messages with a **3** second cooldown`)
}

exports.meta = {
    names: ['frequency'],
    permissions: ['MANAGE_MESSAGES'],
    help: {
        description: 'Set currency generation frequency',
        usage: '15-100',
        category: 'currency'
    }
}