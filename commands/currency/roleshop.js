const { db, functions } = require('../../bot')

exports.run = (m, a) => {
    // WIP
    return functions.respond(m, 'This command is WIP')

    // Create an embed
    const embed = functions.embed()
        .setAuthor('Role shop', m.guild.iconURL)
        .setFooter('Actions: buy, sell, add, remove')

    // Get arguments
    const action = (a[0] || '').toLowerCase()
    const name = a[1]
    const price = Math.ceil(a[2])
    
    // Ensure and filter non-existent roles
    functions.ensure.guild(m.guild.id)
    const roleshop = db.guild.get(m.guild.id, 'roleshop')
        .filter(role => m.guild.roles.has(role.id))

    db.guild.set(m.guild.id, roleshop, 'roleshop')

    // User actions
    if (['buy', 'sell'].includes(action)) {
        if (!name) return functions.respond(m, 'Provide a valid role name')

        if (action == 'buy') {

        } else {

        }
    
    // Admin actions
    } else if (['add', 'remove'].includes(action)) {
        // Check permissions
        if (!m.member.hasPermission('MANAGE_ROLES'))
            return functions.respond(m, 'You need `MANAGE_ROLES` to run this command')

        // Check if the role name is valid
        if (!name) return functions.respond(m, 'Provide a valid role name')

        if (action == 'add') {
            // Find the role
            let roles = m.guild.roles.filter(r => r.name.toLowerCase() == name.toLowerCase())
            if (roles.size > 1) {
                roles = roles.filter(r => r.name == name)
                if (roles.size > 1)
                    return functions.respond(m, 'More than one role with the same name was found. The bot gives up') 
            }
            if (roles.size < 1) return functions.respond(m, 'No roles found. The role name must be exactly the same')
            const role = roles.first()

            // Check if the role is manageable
            if (!role.editable || m.member.highestRole.position <= role.position)
                return functions.respond(m, 'Can\'t manage this role. Please check permissions')

            // Check if the price is valid
            if (!price || isNaN(price) || price < 1)
                return functions.respond(m, 'Provide a valid price')

            if (db.guild.get(m.guild.id, 'roleshop.length') >= 20)
                return functions.respond(m, 'The entry limit (20) was reached')

            if (db.guild.get(m.guild.id, 'roleshop').find(r => r.id == role.id))
                return functions.respond(m, 'This role is already in the shop')

            db.guild.push(m.guild.id, {
                id: role.id,
                price
            },'roleshop')

            embed.setDescription(`Added **${role.name}** to the role shop. Price: ${price}  ${functions.emote()}`)

            m.channel.send({embed})
        } else {
            // Find the role
            let roles = db.guild.get(m.guild.id, 'roleshop').filter(r => r.name.toLowerCase() == name.toLowerCase())
            if (roles.length > 1) roles = roles.filter(r => r.name == name)
            if (roles.length < 1) return functions.respond(m, 'No roles found. The role name must be exactly the same')

            console.log(roles)
        }
    } else {
        let i = 1
        const shop = db.guild.get(m.guild.id, 'roleshop') || []
        const description = shop.map(item => `${i++}. ${item.name} - ${item.price}  ${functions.emote()}`)
            
        embed.setDescription(description.join('\n') || 'None')
        m.channel.send({embed})
    }
}

exports.meta = {
    names: ['roleshop', 'shop', 'store', 'rolestore'],
    permissions: [],
    help: {
        description: 'Use the role shop',
        usage: '[buy/sell/add/remove role-name [price]]',
        category: 'currency'
    }
}