const { bot, db, functions } = require('../../../bot')

exports.run = (m, a) => {
    const user = m.mentions.users.first() || bot.users.get(a[0])
    const amount = Math.ceil(Number(a[1]))
    if (!user || !amount) return functions.respond(m, 'Provide a valid user and amount')

    functions.ensure.user(user.id)
    db.user.math(user.id, '+', amount, 'balance')

    functions.respond(m, `Given **${amount}**  ${functions.emote()}  to **${user.tag}**`)
}

exports.meta = {
    names: ['award'],
    permissions: ['BOT_OWNER'],
    help: {
        description: 'Increase user\'s currency',
        usage: 'amount',
        category: ''
    }
}