const { modules } = require('../../../bot')

exports.run = (m, a) => {
    modules.get('spawnCurrency').run(m, Math.ceil(Number(a[0])) || 1)
}

exports.meta = {
    names: ['spawn'],
    permissions: ['BOT_OWNER'],
    help: {
        description: 'Spawn currency',
        usage: 'amount',
        category: ''
    }
}