const { bot, db, functions } = require('../../../bot')

exports.run = (m, a) => {
    const user = m.mentions.users.first() || bot.users.get(a[0])
    const amount = Math.ceil(Number(a[1]))
    if (!user || isNaN(amount)) return functions.respond(m, 'Provide a valid user and amount')

    functions.ensure.user(user.id)
    db.user.set(user.id, amount, 'balance')

    functions.respond(m, `Set **${user.tag}**'s balance to **${amount}**  ${functions.emote()}`)
}

exports.meta = {
    names: ['setbalance', 'setbal'],
    permissions: ['BOT_OWNER'],
    help: {
        description: 'Set user\'s balance',
        usage: 'amount',
        category: ''
    }
}