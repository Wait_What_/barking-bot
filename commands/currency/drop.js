const { modules, db, functions } = require('../../bot')

exports.run = (m, a) => {
    let amount = Math.ceil(Number(a[0]))
    if (!amount || isNaN(amount) || amount < 1)
        return functions.respond(m, 'Provide a valid amount')

    functions.ensure.user(m.author.id)
    const balance = db.user.get(m.author.id, 'balance') || 0

    if (amount > balance) functions.respond(m, `You only have **${balance}**  ${functions.emote()}`)
    else {
        db.user.math(m.author.id, '-', amount, 'balance')
        modules.get('spawnCurrency').run(m, Math.ceil(amount / 20 * 19))
    }
}

exports.meta = {
    names: ['drop'],
    permissions: [],
    help: {
        description: 'Drop Barkcoin in a channel (5% tax)',
        usage: 'amount',
        category: 'currency'
    }
}