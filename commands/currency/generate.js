const { db, functions, config } = require('../../bot')

exports.run = (m, a) => {
    const channel = m.guild.channels.get((a[0] || '').replace(/#|<|>/g, '')) || m.channel

    functions.ensure.guild(m.guild.id)

    const current = db.guild.get(m.guild.id, 'currency.channels') || []

    if (current.includes(channel.id)) {
        db.guild.remove(m.guild.id, channel.id, 'currency.channels')
        functions.respond(m, `Disabled  ${functions.emote()}  (${config.currency.name}) generation in <#${channel.id}>`)
    } else {
        db.guild.push(m.guild.id, channel.id, 'currency.channels')
        functions.respond(m, `Enabled  ${functions.emote()}  (${config.currency.name}) generation in <#${channel.id}>`)
    }
}

exports.meta = {
    names: ['generate'],
    permissions: ['MANAGE_MESSAGES'],
    help: {
        description: 'Toggle currency generation in a channel',
        usage: '[#channel]',
        category: 'currency'
    }
}