const { db, functions } = require('../../bot')

exports.run = (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0])
    if (!member) return functions.respond(m, 'Mention a valid member')

    const amount = Number(a[1])
    if (!amount || isNaN(amount) || amount < 1) return functions.respond(m, 'Provide a valid amount')

    if (member.user.id == m.author.id) return functions.respond(m, `You can\'t give  ${functions.emote()}  to yourself`)
    if (member.user.bot) return functions.respond(m, 'The user is a bot')

    functions.ensure.user(member.user.id)
    functions.ensure.user(m.author.id)
    const userBalance = db.user.get(m.author.id, 'balance') || 0

    if (amount > userBalance) return functions.respond(m, `You only have **${userBalance}**  ${functions.emote()}`)

    const toGive = Math.ceil(amount / 20 * 19)
    db.user.math(m.author.id, '-', amount, 'balance')
    db.user.math(member.user.id, '+', toGive, 'balance')
    
    const embed = functions.embed()
        .setAuthor(`To: ${member.user.tag}`, member.user.displayAvatarURL)
        .setDescription(`Given **${toGive}** / **${amount}**  ${functions.emote()}  (5% tax)`)
        .setFooter(`From: ${m.author.tag}`, m.author.displayAvatarURL)

    m.channel.send({embed})
}

exports.meta = {
    names: ['give', 'gift'],
    permissions: [],
    help: {
        description: 'Give Barkcoin to a member',
        usage: '@member amount',
        category: 'currency'
    }
}