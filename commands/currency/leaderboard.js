const { bot, db, functions } = require('../../bot')

exports.run = (m, a) => { 
    let i = 1 
    const map = db.user
        .map((user, id) => {
            return { balance: user.balance, id }
        })
        .filter(user => user.balance > 0 && m.guild.members.has(user.id))
        .sort((a, b) => b.balance - a.balance)
        .slice(0, 15)
        .map(user => `${i++}. **${m.guild.members.get(user.id).displayName}** - ${user.balance}`)

    const embed = functions.embed()
        .setAuthor('Top 15 users', bot.user.displayAvatarURL)
        .setDescription(map.join('\n') || 'None')
        .setFooter('Only in this server')

    m.channel.send({embed})
}

exports.meta = {
    names: ['leaderboard', 'top', 'lb'],
    permissions: [],
    help: {
        description: 'See the top 15 users',
        usage: '',
        category: 'currency'
    }
}