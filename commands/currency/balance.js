const { db, functions, config } = require('../../bot')

exports.run = (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0]) || m.member

    functions.ensure.user(member.user.id)
    const balance = db.user.get(member.user.id, 'balance') || 0
    
    const embed = functions.embed()
        .setAuthor(`${member.user.tag}'s balance`, member.user.displayAvatarURL)
        .setDescription(`${balance}  ${functions.emote()}  (${config.currency.name})`)

    m.channel.send({embed})
}

exports.meta = {
    names: ['balance', 'bal', '$'],
    permissions: [],
    help: {
        description: 'See your balance',
        usage: 'amount',
        category: 'currency'
    }
}