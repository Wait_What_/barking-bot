const { functions } = require('../../../bot')
const getRequest = require('../../../library/getRequest')

exports.run = (m, a) => {
    getRequest('https://api.thecatapi.com/v1/images/search')
        .then(r => {
            if (!r || !r[0] || !r[0].url) return functions.respond(m, 'Failed to run the command. Try again later')
            
            const embed = functions.embed()
                .setImage(r[0].url)

            m.channel.send({embed})
        })
        .catch(() => functions.respond(m, 'Failed to run the command. Try again later'))
}

exports.meta = {
    names: ['cat', 'meow', 'nyaa'],
    permissions: [],
    help: {
        description: 'Get a cat image',
        usage: '',
        category: 'image'
    }
}