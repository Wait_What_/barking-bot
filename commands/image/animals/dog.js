const { functions } = require('../../../bot')
const getRequest = require('../../../library/getRequest')

exports.run = (m, a) => {
    getRequest('https://dog.ceo/api/breeds/image/random')
        .then(r => {
            if (!r || !r.message) return functions.respond(m, 'Failed to run the command. Try again later')

            const embed = functions.embed()
                .setImage(r.message)

            m.channel.send({embed})
        })
        .catch(() => functions.respond(m, 'Failed to run the command. Try again later'))
}

exports.meta = {
    names: ['dog', 'woof', 'bark'],
    permissions: [],
    help: {
        description: 'Get a dog image',
        usage: '',
        category: 'image'
    }
}