const { functions } = require('../../../bot')
const { kill } = require('../../../library/resources').anime

exports.run = (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0])
    if (!member) return functions.respond(m, 'Mention someone to kill')
    if (member.user.id == m.author.id)
        return functions.respond(m, 'Please no suicide by bot, thank you')
    
    const embed = functions.embed()
        .setDescription(`${m.author} **killed** ${member}`)
        .setImage(kill.regular[Math.floor(Math.random() * kill.regular.length)])

    m.channel.send({embed})
}

exports.meta = {
    names: ['kill', 'hit'],
    permissions: [],
    help: {
        description: 'Hit someone',
        usage: '@member',
        category: 'image'
    }
}