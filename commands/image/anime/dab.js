const { functions } = require('../../../bot')
const { dab } = require('../../../library/resources').anime

exports.run = (m, a) => {
    const embed = functions.embed()
        .setAuthor(`${m.member.displayName} dabs`, m.author.displayAvatarURL)
        .setImage(dab[Math.floor(Math.random() * dab.length)])

    m.channel.send({embed})
}

exports.meta = {
    names: ['dab'],
    permissions: [],
    help: {
        description: 'Get a dab image',
        usage: '',
        category: 'image'
    }
}