const { functions } = require('../../../bot')
const { kiss } = require('../../../library/resources').anime

exports.run = (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0])
    if (!member) return functions.respond(m, 'Mention someone to kiss')
    
    const image = member.id == m.author.id ? kiss.self[Math.floor(Math.random() * kiss.self.length)] :
        kiss.regular[Math.floor(Math.random() * kiss.regular.length)]

    const embed = functions.embed()
        .setDescription(`${m.author} **kissed** ${member}`)
        .setImage(image)

    m.channel.send({embed})
}

exports.meta = {
    names: ['kiss'],
    permissions: [],
    help: {
        description: 'Kiss someone',
        usage: '@member',
        category: 'image'
    }
}