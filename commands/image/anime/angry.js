const { functions } = require('../../../bot')
const { angry } = require('../../../library/resources').anime

exports.run = (m, a) => {
    const embed = functions.embed()
        .setAuthor(`${m.member.displayName} is angry`, m.author.displayAvatarURL)
        .setImage(angry[Math.floor(Math.random() * angry.length)])

    m.channel.send({embed})
}

exports.meta = {
    names: ['angry'],
    permissions: [],
    help: {
        description: 'Get an angry image',
        usage: '',
        category: 'image'
    }
}