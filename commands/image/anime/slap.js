const { functions } = require('../../../bot')
const { slap } = require('../../../library/resources').anime

exports.run = (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0])
    if (!member) return functions.respond(m, 'Mention someone to slap')
    
    const embed = functions.embed()
        .setDescription(`${m.author} **slapped** ${member}`)
        .setImage(slap.regular[Math.floor(Math.random() * slap.regular.length)])

    m.channel.send({embed})
}

exports.meta = {
    names: ['slap'],
    permissions: [],
    help: {
        description: 'Slap someone',
        usage: '@member',
        category: 'image'
    }
}