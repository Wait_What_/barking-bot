const { functions } = require('../../../bot')
const { pat } = require('../../../library/resources').anime

exports.run = (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0])
    if (!member) return functions.respond(m, 'Mention someone to pat')
    
    const embed = functions.embed()
        .setDescription(`${m.author} **patted** ${member}`)
        .setImage(pat.regular[Math.floor(Math.random() * pat.regular.length)])

    m.channel.send({embed})
}

exports.meta = {
    names: ['pat', 'headpat'],
    permissions: [],
    help: {
        description: 'Pat someone',
        usage: '@member',
        category: 'image'
    }
}