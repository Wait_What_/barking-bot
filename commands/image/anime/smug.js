const { functions } = require('../../../bot')
const { smug } = require('../../../library/resources').anime

exports.run = (m, a) => {
    const embed = functions.embed()
        .setAuthor(`${m.member.displayName} makes a smug face!`, m.author.displayAvatarURL)
        .setImage(smug[Math.floor(Math.random() * smug.length)])

    m.channel.send({embed})
}

exports.meta = {
    names: ['smug'],
    permissions: [],
    help: {
        description: 'Get a smug image',
        usage: '',
        category: 'image'
    }
}