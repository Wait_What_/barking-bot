const { functions } = require('../../../bot')
const { hug } = require('../../../library/resources').anime

exports.run = (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0])
    if (!member) return functions.respond(m, 'Mention someone to hug')
    
    const embed = functions.embed()
        .setDescription(`${m.author} **hugged** ${member}`)
        .setImage(hug.regular[Math.floor(Math.random() * hug.regular.length)])

    m.channel.send({embed})
}

exports.meta = {
    names: ['hug', 'cuddle'],
    permissions: [],
    help: {
        description: 'Hug someone',
        usage: '@member',
        category: 'image'
    }
}