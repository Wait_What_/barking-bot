const { functions } = require('../../../bot')
const { disgust } = require('../../../library/resources').anime

exports.run = (m, a) => {
    const embed = functions.embed()
        .setAuthor(`${m.member.displayName} is disgusted by this`, m.author.displayAvatarURL)
        .setImage(disgust[Math.floor(Math.random() * disgust.length)])

    m.channel.send({embed})
}

exports.meta = {
    names: ['disgust', 'disgusting', 'disgusted'],
    permissions: [],
    help: {
        description: 'Get a disgusted image',
        usage: '',
        category: 'image'
    }
}