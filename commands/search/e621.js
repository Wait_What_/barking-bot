const { functions } = require('../../bot')

let booru
try { booru = require('booru') }
catch { console.log('`booru` wasn\'t found. The `*booru` commands are disabled')}

exports.run = (m, a) => {
    if (!booru) return functions.respond(m, 'The *booru commands are disabled')
    if (a.length < 1) return functions.respond(m, 'Provide some tags')
    if (!m.channel.nsfw) return functions.respond(m, 'This command is only available in NSFW channels')

    a = [...a.filter(arg => !arg.match(/cub/gi) || !arg.match(/rating/gi)), '-cub', 'rating:s']
    booru.search('e621', a, { limit: 1, random: true }).then(res => {
        if (!res[0]) return functions.respond(m, 'No results')

        const embed = functions.embed()
            .setImage(res[0].fileUrl)
            .setFooter('Results from e621')

        m.channel.send({embed})
    }).catch(() => functions.respond(m, 'No results') )
}

exports.meta = {
    names: ['e621', 'e6', 'furry'],
    permissions: [],
    help: {
        description: 'Search for images on e621',
        usage: 'tags',
        category: 'search'
    }
}