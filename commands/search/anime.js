const { functions } = require('../../bot')

let anifetch
try { anifetch = require('anifetch') }
catch { console.log('`anifetch` wasn\'t found. The `anime` command is disabled')}

exports.run = (m, a) => {
    if (!anifetch) return functions.respond(m, 'The anime command is disabled')

    const request = a.join(' ')
    if (!request) return functions.respond(m, 'Provide a valid request')

    m.channel.startTyping()

    anifetch('MyAnimeList', 'anime', request).then(res => {
        if (!res[0]) return functions.respond(m, 'No results')
        
        const embed = functions.embed()
            .setAuthor(res[0].title_canonical, res[0].provider_avatar, res[0].url)
            .setDescription(`${res[0].episodes || '-'} episodes | Rating: ${res[0].score || '-'} / 100`)
            .addField('Description', `${res[0].synopsis.slice(0, 200)}${res[0].synopsis.length > 200 ? '...' : ''}`)
            .setThumbnail(res[0].cover)
            .setFooter(`${res[0].format || '-'} | ${res[0].type || '-'} | ID: ${res[0].id || '-'}`)

        m.channel.stopTyping()
        m.channel.send({embed})
    }).catch(() => {
        m.channel.stopTyping()
        functions.respond(m, 'No results')
    })
}

exports.meta = {
    names: ['anime', 'mal'],
    permissions: [],
    help: {
        description: 'Search for anime on MyAnimeList',
        usage: 'anime-name',
        category: 'search'
    }
}