const { functions } = require('../../bot')
const getRequest = require('../../library/getRequest')

exports.run = async (m, a) => {
    if (!m.channel.nsfw) return functions.respond(m, 'This command is only available in NSFW channels')

    if (!a[0]) return functions.respond(m, 'Provide a valid request')
    if (a.join(' ').length > 128) return functions.respond('The request is too long (up to 128 characters)')

    m.channel.startTyping()

    getRequest('https://api.urbandictionary.com/v0/define?term=' + a.join('%20'))
        .then(r => {
            if (!r.list || !r.list[0]) {
                m.channel.stopTyping()
                functions.respond(m, 'No results')
                return
            }

            const definition = `${r.list[0].definition.replace(/\[|\]/g, '').slice(0, 256)}${r.list[0].definition.length > 256 ? '...' : ''}`
            const example = `${r.list[0].example.replace(/\[|\]/g, '').slice(0, 256)}${r.list[0].example.length > 256 ? '...' : ''}`

            const embed = functions.embed()
                .setAuthor(a.join(' '), 'https://i.imgur.com/xq2FCVq.png', r.list[0].permalink)
                .addField('Definition', definition || '-')
                .setFooter(`${r.list[0].thumbs_up || '-'} likes | Author: ${r.list[0].author || '-'} | ID: ${r.list[0].defid}`)

            if (example) embed.addField('Example', example)

            m.channel.stopTyping()
            m.channel.send({embed})
        })
        .catch(() => {
            m.channel.stopTyping()
            functions.respond(m, 'No results')
        })
}

exports.meta = {
    names: ['urban', 'ud', 'define'],
    permissions: [],
    help: {
        description: 'Search on Urban Dictionary',
        usage: 'request',
        category: 'search'
    }
}