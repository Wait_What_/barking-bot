const { functions } = require('../../bot')
let ytSearch
try { ytSearch = require('yt-search') }
catch { console.log('`yt-search` wasn\'t found. The `youtube` command is disabled') }

exports.run = (m, a) => {
    if (!ytSearch) return functions.respond(m, 'The youtube command is disabled')
    
    const request = a.join(' ')
    if (request.length > 128) return functions.respond(m, 'The request is too long')
    if (!request) return functions.respond(m, 'Provide a valid request')

    m.channel.send(`Searching for **${request}**...`).then(m2 =>
        ytSearch(request, (err, r) => {
            if (err || !r.videos[0]) return m2.edit('No results')
            else m2.edit(`https://www.youtube.com/watch?v=${r.videos[0].videoId}`)
        })
    )
}

exports.meta = {
    names: ['youtube', 'yt'],
    permissions: [],
    help: {
        description: 'Search on YouTube',
        usage: 'request',
        category: 'search'
    }
}