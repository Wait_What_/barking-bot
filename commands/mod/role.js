const { functions } = require('../../bot')

exports.run = async (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0])
    if (!member) return functions.respond(m, 'Mention a valid member')
    
    const name = a.slice(1).join(' ')
    if (name.length > 999) return functions.respond(m, 'The role name is too long (up to 999 characters)')
    if (!name) return functions.respond(m, 'Provide a valid role name')

    let roles = m.guild.roles.filter(r => r.name.toLowerCase() == name.toLowerCase())
    if (roles.size > 1) {
        roles = roles.filter(r => r.name == name)
        if (roles.size > 1)
            return functions.respond(m, 'More than one role with the same name was found. The bot gives up') 
    }
    if (roles.size < 1) return functions.respond(m, 'No roles found. The role name must be exactly the same')

    const role = roles.first()

    if (!member.manageable ||
        member.highestRole.position >= m.member.highestRole.position ||
        m.member.highestRole.position <= role.position)
            return functions.respond(m, 'Can\'t manage this role. Please check permissions')

    const embed = functions.embed()
        .setAuthor(member.user.tag, member.user.displayAvatarURL)
        .setFooter(`Moderator: ${m.author.tag}`, m.author.displayAvatarURL)

    if (member.roles.has(role.id)) member.removeRole(role.id, `Added by ${m.author.tag}`)
        .then(() => {
            embed.setDescription(`Removed role **${role.name}**`)
            m.channel.send({embed})
        })
        .catch(() => functions.respond(m, 'Couldn\'t remove the role. Please check permissions'))

    else member.addRole(role.id, `Added by ${m.author.tag}`)
        .then(() => {
            embed.setDescription(`Added role **${role.name}**`)
            m.channel.send({embed})
        })
        .catch(() => functions.respond(m, 'Couldn\'t add the role. Please check permissions'))
}

exports.meta = {
    names: ['role'],
    permissions: ['MANAGE_ROLES'],
    help: {
        description: 'Add/remove a role',
        usage: '@member role',
        category: 'mod'
    }
}