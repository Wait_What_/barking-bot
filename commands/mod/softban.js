const { functions } = require('../../bot')

exports.run = (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0])
    if (!member) return functions.respond('Mention a valid member')

    const reason = a.slice(1).join(' ') || 'No reason provided'
    if (reason.length > 450) return functions.respond(m, 'The reason provided is too long')

    if (member.bannable && 0 > member.highestRole.comparePositionTo(m.member.highestRole)) {
        const embed = Bark.embed()
            .setAuthor(member.user.tag, member.user.displayAvatarURL)
            .addField(`Softanned from ${m.guild.name}`, `Reason: ${reason}`)
            .setFooter(`Moderator: ${m.author.tag}`, m.author.displayAvatarURL)

        member.send({embed})
            .catch(() => {})
            .then(() => {
                member.ban({
                    reason: `Softbanned by ${m.member.displayName} for: ${reason}`,
                    days: 7
                }).catch(() => functions.respond(m, 'Couldn\'t ban this member'))
                    .then(() => {
                        m.guild.unban(member.id)
                            .catch(() => functions.respond(m, 'Failed to unban this member, please check permissions'))
            
                        m.channel.send({embed})
                    })
            }) 
    } else functions.respond(m, 'Can\'t ban this member')
}

exports.meta = {
    names: ['softban', 'sb'],
    permissions: ['BAN_MEMBERS'],
    help: {
        description: 'Ban and instantly unban a member',
        usage: '@member [reason]',
        category: 'mod'
    }
}