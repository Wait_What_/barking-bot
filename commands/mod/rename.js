const { functions } = require('../../bot')

exports.run = async (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0])
    if (!member) return functions.respond(m, 'Mention a valid member')
    
    const name = a.slice(1).join(' ')
    if (name.length > 32) return functions.respond(m, 'The nickname is too long (up to 32 characters)')

    if (member.manageable) {
        member.setNickname(name)
            .catch(() => functions.respond(m, 'Couldn\'t rename the member. Please check permissions'))
            .then(() => {
                const embed = functions.embed()
                    .setAuthor(member.user.tag, member.user.displayAvatarURL)
                    .addField('Updated nickname', name || 'Reset nickname')
                    .setFooter(`Moderator: ${m.author.tag}`, m.author.displayAvatarURL)
        
                m.channel.send({embed})
            })
        
    } else functions.respond(m, 'Can\'t rename the member. Please check permissions')
}

exports.meta = {
    names: ['rename', 'nick', 'nickname'],
    permissions: ['MANAGE_NICKNAMES'],
    help: {
        description: 'Change a member\'s nickname',
        usage: '@member [name]',
        category: 'mod'
    }
}