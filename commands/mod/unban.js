const { functions } = require('../../bot')

exports.run = (m, a) => {
    const member = a[0]
    if (!member || member.toString().length < 17 || member.toString().length > 19 || isNaN(member))
        return functions.respond(m, 'Provide a valid user id')

    const reason = a.slice(1).join(' ') || 'No reason provided'
    if (reason.length > 450) return functions.respond(m, 'The reason provided is too long')

    if (m.guild.members.has(member)) return functions.respond(m, 'They are already in the server, no need to unban')

    const embed = functions.embed()
        .setAuthor(member, m.guild.iconURL)
        .addField(`Unbanned from ${m.guild.name}`, `Reason: ${reason}`)
        .setFooter(`Moderator: ${m.author.tag}`, m.author.displayAvatarURL)

    m.guild.unban(member, `Unbanned by ${m.member.displayName} for: ${reason}`)
        .catch(() => functions.respond(m, 'Couldn\'t unban this user. Please check permissions'))
        
    m.channel.send({embed})
}

exports.meta = {
    names: ['unban', 'ub'],
    permissions: ['BAN_MEMBERS'],
    help: {
        description: 'Unban a banned user',
        usage: 'user-id [reason]',
        category: 'mod'
    }
}