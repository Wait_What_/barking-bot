const { functions } = require('../../bot')

exports.run = async (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0])
    const number = Number(a[0])

    if (!member)
        if (isNaN(number) || !number || number > 99 || number < 1)
           return functions.respond(m, 'Use a valid number from 1 to 99 or mention a valid member')

    if (member) {
        const messages = await m.channel.fetchMessages({ limit: 100 })
        const collection = messages.filter(msg => msg.author.id == member.user.id)
        await m.channel.bulkDelete(collection)

        const embed = functions.embed()
            .setDescription(`Cleaned ${collection.size} messages by ${member.user.tag}!`)

        m.channel.send({embed}).then(m2 => m2.delete(600))
    } else if (number) {
        await m.channel.bulkDelete(number + 1)

        const embed = functions.embed()
            .setDescription(`Cleaned ${number} messages!`)

        m.channel.send({embed}).then(m2 => m2.delete(300))
    } else return functions.respond('Use a number from 1 to 99 or mention a member!')     
}

exports.meta = {
    names: ['clear', 'purge', 'clean', 'delete', 'c'],
    permissions: ['MANAGE_MESSAGES'],
    help: {
        description: 'Bulk delete messages',
        usage: '1-99 / @member',
        category: 'mod'
    }
}