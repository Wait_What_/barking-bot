const { db, functions } = require('../../bot')

exports.run = (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0])

    functions.ensure.guild(m.guild.id)
    const snipe = db.guild.get(m.guild.id, 'snipe')[m.channel.id]

    if (!snipe || !snipe.last) return functions.respond(m, 'No saved messages in this channel')

    const embed = functions.embed()
        .setFooter(`Moderator: ${m.author.tag}`, m.author.displayAvatarURL)

    if (member) {
        if (!snipe[member.user.id]) return functions.respond(m, 'No saved messages from this member')
        embed.setAuthor(member.user.tag, member.user.displayAvatarURL)
        embed.setDescription(snipe[member.user.id])
    }
    else {
        const sniped = m.guild.members.get(snipe.last)
        if (!sniped) return functions.respond(m, 'The member is no longer in this server')

        embed.setAuthor(sniped.user.tag, sniped.user.displayAvatarURL)
        embed.setDescription(snipe[snipe.last])
    }

    m.channel.send({embed})
}

exports.meta = {
    names: ['undelete', 'snipe'],
    permissions: ['MANAGE_MESSAGES'],
    help: {
        description: 'Restore the last deleted message',
        usage: '[@member]',
        category: 'mod'
    }
}