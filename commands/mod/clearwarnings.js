const { db, functions } = require('../../bot')

exports.run = async (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0])
    if (!member) return functions.respond(m, 'Mention a valid member')
    
    functions.ensure.guild(m.guild.id)

    const warnings = db.guild.get(m.guild.id, 'warnings')
    warnings[member.user.id] = []

    db.guild.set(m.guild.id, warnings, 'warnings')

    const embed = functions.embed()
        .setAuthor(member.user.tag, member.user.displayAvatarURL)
        .setDescription('Cleared warnings')
        .setFooter(`Moderator: ${m.author.tag}`, m.author.displayAvatarURL)

    m.channel.send({embed})
}

exports.meta = {
    names: ['clearwarnings', 'clearwarning', 'cw'],
    permissions: ['KICK_MEMBERS'],
    help: {
        description: 'Clear a member\'s warnings',
        usage: '@member',
        category: 'mod'
    }
}