const { db, functions } = require('../../bot')

exports.run = async (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0])
    if (!member) return functions.respond(m, 'Mention a valid member')

    const reason = a.slice(1).join(' ')
    if (reason.length > 450) return functions.respond(m, 'The reason provided is too long')
    if (!reason) return functions.respond(m, 'Provide a valid reason')

    if (member.kickable && 0 > member.highestRole.comparePositionTo(m.member.highestRole)) {
        functions.ensure.guild(m.guild.id)

        const warnings = db.guild.get(m.guild.id, 'warnings')
        if (!warnings[member.user.id]) warnings[member.user.id] = []

        warnings[member.user.id].push({
            time: Date.now(),
            mod: m.author.id,
            reason: reason
        })

        db.guild.set(m.guild.id, warnings, 'warnings')

        const embed = functions.embed()
            .setAuthor(member.user.tag, member.user.displayAvatarURL)
            .addField(`Warned in ${m.guild.name}`, `Warning **#${warnings[member.user.id].length}**\nReason: ${reason}`)
            .setFooter(`Moderator: ${m.author.tag}`, m.author.displayAvatarURL)

        if (warnings[member.user.id].length >= 3) {
            await member.send({embed}).catch(() => {})

            member.kick(`Kicked after the ${warnings[member.user.id].length} warning by ${m.author.tag}`)
                .catch(() => {
                    embed.addField('Failed to kick', 'This was the 3rd warning, but the bot ' +
                        'couldn\'t kick this member. Please check permissions')

                    member.send({embed}).catch(() => {})
                    m.channel.send({embed})
                })
                .then(() => {
                    embed.addField('Kicked', 'The member was kicked after 3 warnings')

                    member.send({embed}).catch(() => {})
                    m.channel.send({embed})

                    db.guild.set(m.guild.id, [], `warnings.${member.user.id}`)
                })
            return
        }

        member.send({embed}).catch(() => {})
        m.channel.send({embed})
    } else functions.respond(m, 'Can\'t warn that member. Please check permissons')
}

exports.meta = {
    names: ['warn', 'w'],
    permissions: ['KICK_MEMBERS'],
    help: {
        description: 'Warn a member',
        usage: '@member reason',
        category: 'mod'
    }
}