const { functions } = require('../../bot')

exports.run = (m, a) => {
    const member = m.mentions.members.first() || m.guild.members.get(a[0])
    if (!member) return functions.respond(m, 'Mention a valid member')

    const reason = a.slice(1).join(' ') || 'No reason provided'
    if (reason.length > 450) return functions.respond(m, 'The reason provided is too long')

    if (member.kickable && 0 > member.highestRole.comparePositionTo(m.member.highestRole)) {
        const embed = functions.embed()
            .setAuthor(member.user.tag, member.user.displayAvatarURL)
            .addField(`Kicked from ${m.guild.name}`, `Reason: ${reason}`)
            .setFooter(`Moderator: ${m.author.tag}`, m.author.displayAvatarURL)

        member.send({embed})
            .catch(() => {})
            .then(() => {
                member.kick(`Kicked by ${m.member.displayName} for: ${reason}`)
                    .catch(() => functions.respond(m, 'Can\'t kick that member. Please check permissons'))
                    .then(() => m.channel.send({embed}))
            })
    } else functions.respond(m, 'Can\'t kick that member. Please check permissons')
}

exports.meta = {
    names: ['kick', 'k'],
    permissions: ['KICK_MEMBERS'],
    help: {
        description: 'Kick a member',
        usage: '@member [reason]',
        category: 'mod'
    }
}