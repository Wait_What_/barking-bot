const { db, functions } = require('../../bot')

exports.run = async (m, a) => {
    const parseDate = date => new Date(date).toISOString().split('.')[0].replace('T', ' ')

    const member = m.mentions.members.first() || m.guild.members.get(a[0])
    if (!member) return functions.respond(m, 'Mention a valid member')   

    const embed = functions.embed()
        .setAuthor(member.user.tag, member.user.displayAvatarURL)
        .setFooter(`Moderator: ${m.author.tag}`, m.author.displayAvatarURL)
    
    functions.ensure.guild(m.guild.id) 

    const warnings = db.guild.get(m.guild.id, 'warnings')[member.user.id]
    if (!warnings || warnings.length < 1) embed.setDescription('No warnings')
    else {
        let i = 1
        const description = warnings.map(warning => `${i++}. **${parseDate(warning.time)}** by ` +
            `**${m.guild.members.has(warning.mod) ? m.guild.members.get(warning.mod).user.tag : 'Unknown'}**` +
            `: \`${warning.reason || 'Unknown'}\``)

        embed.setDescription(`${warnings.length} warning${warnings.length > 1 ? 's' : ''}:` +
            `\n${description.join('\n') || 'Unknown'}`)
    }

    m.channel.send({embed})
}

exports.meta = {
    names: ['warnings', 'warns'],
    permissions: ['KICK_MEMBERS'],
    help: {
        description: 'See a member\'s warnings',
        usage: '@member',
        category: 'mod'
    }
}