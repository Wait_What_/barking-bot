# BarkingBot

[![MIT](https://img.shields.io/badge/License-MIT-blue.svg?style=flat-square)](https://gitlab.com/BarkingDog/barking-bot/blob/master/LICENSE.md)
[![NODE](https://img.shields.io/badge/Language-node.js-brightgreen.svg?style=flat-square)](https://nodejs.org/en/)
[![SUPPORTSERVER](https://img.shields.io/badge/Support%20server-Join-yellow.svg?style=flat-square)](https://discord.gg/N8Fqcuk)

## [INVITE](https://waitwhat.xyz/bb/inv) | [VOTE](https://waitwhat.xyz/bb/vote)

A discord bot built using `discord-bot-boilerplate`

## Where do I start?
Read [Usage](DOCS/usage.md) for an **install and basic usage guide**

Check out [DOCS](DOCS/) for full documentation, including a [list of commands](DOCS/commands.md)

## Support
Discord [support server](https://discord.gg/N8Fqcuk)

Add me: `BarkingDog#4975`

## License
The bot is licensed under [MIT](LICENSE.md)

## EULA
By using this bot you agree that some data may be collected: 

- Your user ID will be saved if you use some of the commands such as `balance`
- The last deleted message in a channel for the `undelete` command
- Your user ID will be saved if you vote for the bot
- Your and the moderator's user IDs if the `warn` command is used
- ...

This will never be shared and it's required for the bot to function properly

Data that is NOT collected:

- Your username and tag
- Your regular messages
