const { bot, db, functions } = require('../../bot')

exports.run = () => {
    bot.on('messageDelete', m => {
        if (m.channel.type != 'text' || m.author.bot) return
        functions.ensure.guild(m.guild.id)

        const snipe = db.guild.get(m.guild.id, 'snipe')
        if (!snipe[m.channel.id]) snipe[m.channel.id] = {}

        snipe[m.channel.id][m.author.id] = m.content
        snipe[m.channel.id].last = m.author.id

        const keys = Object.keys(snipe[m.channel.id])
        if (keys.length > 5) 
            delete snipe[m.channel.id][keys[0]]

        db.guild.set(m.guild.id, snipe, 'snipe')
    })
}

exports.meta = {
    name: 'snipe',
    autorun: 4
}