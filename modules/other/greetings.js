const { bot, db, functions } = require('../../bot')

exports.run = () => {
    bot.on('guildMemberAdd', m => {
        functions.ensure.guild(m.guild.id)
        const greetings = db.guild.get(m.guild.id, 'greetings')

        const text = greetings.join.text
            .replace(/{{membercount}}/gi, m.guild.memberCount)
            .replace(/{{memberping}}/gi, `<@${m.user.id}>`)
            .replace(/{{membertag}}/gi, m.user.tag)
            .replace(/{{membername}}/gi, m.displayName)
            .replace(/{{servername}}/gi, m.guild.name)

        if (greetings.join.text) {
            if (greetings.embed) {
                const embed = functions.embed()
                    .setAuthor(m.guild.name, m.guild.iconURL)
                    .setDescription(text)
                    .setTimestamp(Date.now())

                if (greetings.dm) m.send({embed}).catch(() => {})
                if (greetings.join.channel && m.guild.channels.has(greetings.join.channel))
                    m.guild.channels.get(greetings.join.channel).send({embed}).catch(() => {})
            } else {
                if (greetings.dm) m.send(text).catch(() => {})
                if (greetings.join.channel && m.guild.channels.has(greetings.join.channel))
                    m.guild.channels.get(greetings.join.channel).send(text).catch(() => {})
            }
        }
    })

    bot.on('guildMemberRemove', m => {
        functions.ensure.guild(m.guild.id)
        const goodbyes = db.guild.get(m.guild.id, 'greetings')

        const text = goodbyes.leave.text
            .replace(/{{membercount}}/gi, m.guild.memberCount)
            .replace(/{{membertag}}/gi, m.user.tag)
            .replace(/{{membername}}/gi, m.displayName)
            .replace(/{{servername}}/gi, m.guild.name)

        if (goodbyes.leave.text) {
            if (goodbyes.embed) {
                const embed = functions.embed()
                    .setAuthor(m.guild.name, m.guild.iconURL)
                    .setDescription(text)
                    .setTimestamp(Date.now())

                if (goodbyes.leave.channel && m.guild.channels.has(goodbyes.leave.channel))
                    m.guild.channels.get(goodbyes.leave.channel).send({embed}).catch(() => {})
            } else if (goodbyes.leave.channel && m.guild.channels.has(goodbyes.leave.channel))
                m.guild.channels.get(goodbyes.leave.channel).send(text).catch(() => {})
        }
    })
}

exports.meta = {
    name: 'greetings',
    autorun: 5
}