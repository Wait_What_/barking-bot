const { bot, db, functions, config } = require('../../bot')

exports.run = (m, amount) => {
    const titles = ['Woah!', 'Wow!', 'Epic!', 'Hey!', 'Look!']

    const a = Math.round(Math.random() * 9 + 1)
    const b = Math.round(Math.random() * 9 + 1)
    
    const question = Math.random() < 0.8 ? (
        Math.random() > 0.5 ? `${a}** + **${b}` : `${a + 10}** - **${b}`
    ) : `${a}** \* **${b}`
    const answer = question.includes('+') || question.includes('-') ? (
        question.includes('+') ? a + b : a + 10 - b
    ) : a * b

    const embed = functions.embed()
        .setAuthor(titles[Math.floor(Math.random() * titles.length)], bot.user.displayAvatarURL)
        .addField(`**${amount}**  ${functions.emote()}  appeared!`,
            `Solve **${question}**\nSay **\`pick [answer]\`** to get ${amount == 1 ? 'it' : 'them'}!`)
        .setThumbnail(config.currency.icon)

    m.channel.send({embed}).then(m2 => {
        m2.channel.awaitMessages(m3 => {
            functions.ensure.user(m3.author.id)
            return !m3.author.bot &&
                m3.content.toLowerCase().replace(/ /g, '') == `pick${answer}` &&
                !db.user.get(m3.author.id, 'blacklist')
        }, { max: 1 }).then(collected => {
            const m3 = collected.first()

            m2.delete().catch(() => {})
            m3.delete().catch(() => {})

            db.user.math(m3.author.id, '+', amount, 'balance')

            const embed2 = functions.embed()
                .setDescription(`**${m3.member.displayName}** picked up ${amount}  ${functions.emote()}`)

            m2.channel.send({embed: embed2})
                .then(m4 => m4.delete(3000).catch(() => {}))
        })
    })
}

exports.meta = {
    name: 'spawnCurrency',
    autorun: 0
}