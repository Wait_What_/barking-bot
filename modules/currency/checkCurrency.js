const { modules, db, functions } = require('../../bot')

const counter = {}
const timeouts = {}

exports.run = m => {
    functions.ensure.guild(m.guild.id)

    if (!timeouts[m.author.id]) timeouts[m.author.id] = Date.now() - 99999
    if (Date.now() - timeouts[m.author.id] < 3000) return
    else timeouts[m.author.id] = Date.now()

    const channels = db.guild.get(m.guild.id, 'currency.channels') || []
    const frequency = db.guild.get(m.guild.id, 'currency.frequency') || 0
    if (!channels.includes(m.channel.id) || frequency < 1) return

    if (!counter[m.guild.id]) counter[m.guild.id] = {}
    if (counter[m.guild.id][m.channel.id] == undefined) counter[m.guild.id][m.channel.id] = 0

    if (counter[m.guild.id][m.channel.id] >= frequency && Math.random() < 0.2) {
        modules.get('spawnCurrency').run(m, Math.floor(Math.random() * 49) + 1)

        counter[m.guild.id][m.channel.id] = 0
    } else counter[m.guild.id][m.channel.id]++
}

exports.meta = {
    name: 'checkCurrency',
    autorun: 0
}