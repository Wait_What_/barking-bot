module.exports = {
    anime: {
        kiss: {
            regular: [
                'https://i.imgur.com/Wv0ZTkE.gif',
                'https://i.imgur.com/k8LZcqj.gif',
                'https://i.imgur.com/NnobY6C.gif',
                'https://i.imgur.com/hkR6hyF.gif',
                'https://i.imgur.com/SWImK6c.gif',
                'https://i.imgur.com/ypFtLy0.gif',
                'https://i.imgur.com/pquXE3p.gif',
                'https://i.imgur.com/N10u3xm.gif',
                'https://i.imgur.com/MrmzP3n.gif',
                'https://i.imgur.com/7PH6i38.gif',
                'https://i.imgur.com/rLGMByi.gif',
                'https://i.imgur.com/EgU30CH.gif',
                'https://i.imgur.com/DUIDiWS.gif',
                'https://i.imgur.com/IjcWt89.gif',
                'https://i.imgur.com/HXZ4xQv.gif',
                'https://i.imgur.com/ivmr7yA.gif',
                'https://i.imgur.com/QpmEMcp.gif',
                'https://i.imgur.com/6BWeAW2.gif',
                'https://i.imgur.com/0UiSLas.gif',
                'https://i.imgur.com/ry4jf78.gif',
                'https://i.imgur.com/NwUfJHR.gif',
                'https://i.imgur.com/6InAeG9.gif'
            ],
            self: [
                'https://i.imgur.com/xbu1Lyu.gif'
            ]
        },
        slap: {
            regular: [
                'https://i.imgur.com/N5oDbhf.gif',
                'https://i.imgur.com/4a5MVMu.gif',
                'https://i.imgur.com/BkGyzU4.gif',
                'https://i.imgur.com/Y4G4qoG.gif',
                'https://i.imgur.com/DvrcYaW.gif',
                'https://i.imgur.com/roePZMp.gif',
                'https://i.imgur.com/1aPp4Lc.gif',
                'https://i.imgur.com/4PJa1F0.gif',
                'https://i.imgur.com/5qyCl8A.gif',
                'https://i.imgur.com/gdQgeOX.gif',
                'https://i.imgur.com/0Uj3VSU.gif',
                'https://i.imgur.com/kUHL1ca.gif',
                'https://i.imgur.com/geFNw07.gif',
                'https://i.imgur.com/W6icDyh.gif',
                'https://i.imgur.com/HXakcyl.gif',
                'https://i.imgur.com/O3mDgiq.gif',
                'https://i.imgur.com/wnYuGsU.gif'
            ]
        },
        hug: {
            regular: [
                'https://i.imgur.com/zmD97fg.gif',
                'https://i.imgur.com/k9y9ZRz.gif',
                'https://i.imgur.com/1GfZjPk.gif',
                'https://i.imgur.com/6pvuIgQ.gif',
                'https://i.imgur.com/QZGyajy.gif',
                'https://i.imgur.com/RVGH2Qo.gif',
                'https://i.imgur.com/eyRLCtG.gif',
                'https://i.imgur.com/E26XhcC.gif',
                'https://i.imgur.com/rrT3aMW.gif',
                'https://i.imgur.com/SdFDQxT.gif',
                'https://i.imgur.com/dUPbmCr.gif',
                'https://i.imgur.com/jGhX6nt.gif',
                'https://i.imgur.com/4XG8dzC.gif',
                'https://i.imgur.com/8V6gkxU.gif',
                'https://i.imgur.com/2jeFgim.gif',
                'https://i.imgur.com/j8NIe1q.gif',
                'https://i.imgur.com/hvjwWBN.gif',
                'https://i.imgur.com/nGSUDPq.gif',
                'https://i.imgur.com/ejKlB4X.gif',
                'https://i.imgur.com/HqRThOd.gif',
                'https://i.imgur.com/AcbAC82.gif',
                'https://i.imgur.com/dKgiTB2.gif',
                'https://i.imgur.com/ZgaSlSp.gif'
            ]
        },
        pat: {
            regular: [
                'https://i.imgur.com/OBhfp2M.gif',
                'https://i.imgur.com/wJJwoTk.gif',
                'https://i.imgur.com/G1IinwH.gif',
                'https://i.imgur.com/V14OhmU.gif',
                'https://i.imgur.com/t8QGjGw.gif',
                'https://i.imgur.com/deOTRKu.gif',
                'https://i.imgur.com/eiXYeoO.gif',
                'https://i.imgur.com/PMgOm6s.gif',
                'https://i.imgur.com/5klHs8z.gif'
            ]
        },
        kill: {
            regular: [
                'https://i.imgur.com/ggsLnCf.gif',
                'https://i.imgur.com/5eYnQL2.gif',
                'https://i.imgur.com/lLhmTft.gif',
                'https://i.imgur.com/tdy2zB6.gif'
            ]
        },
        smug: [
            'https://i.imgur.com/XnYK3iv.jpg',
            'https://i.imgur.com/21xMaAm.png',
            'https://i.imgur.com/BMA5yyO.png',
            'https://i.imgur.com/kOCV05j.png',
            'https://i.imgur.com/ukQyTDy.jpg',
            'https://i.imgur.com/bmQZWj2.jpg',
            'https://i.imgur.com/NZ7Yw7j.png',
            'https://i.imgur.com/4CHxtax.png',
            'https://i.imgur.com/boW0xEp.png',
            'https://i.imgur.com/paMjUXM.png',
            'https://i.imgur.com/nKySMEj.jpg',
            'https://i.imgur.com/S6QrkuH.jpg',
            'https://i.imgur.com/ZIRqeF6.png',
            'https://i.imgur.com/MQNH7X7.jpg',
            'https://i.imgur.com/4FMlLWU.jpg',
            'https://i.imgur.com/FraVcUv.png',
            'https://i.imgur.com/VTStBZm.jpg',
            'https://i.imgur.com/Pu7Czba.jpg',
            'https://i.imgur.com/bqPnT8n.jpg',
            'https://i.imgur.com/CPUwQ4s.jpg',
            'https://i.imgur.com/6tmIflH.jpg',
            'https://i.imgur.com/4W7Lb1k.jpg',
            'https://i.imgur.com/qEyKDFx.jpg',
            'https://i.imgur.com/bsgV6sq.png',
            'https://i.imgur.com/QwJ2b26.jpg',
            'https://i.imgur.com/6k0mKiW.png',
            'https://i.imgur.com/5Id5btm.jpg',
            'https://i.imgur.com/df0UPuE.jpg'
        ],
        angry: [
            'https://i.imgur.com/DcvuKdn.jpg',
            'https://i.imgur.com/NSzEvjr.png',
            'https://i.imgur.com/Mf6AVs3.jpg',
            'https://i.imgur.com/rQooNfY.jpg',
            'https://i.imgur.com/Adu5Xtq.jpg',
            'https://i.imgur.com/py5YhYN.png',
            'https://i.imgur.com/ibkqgGv.png',
            'https://i.imgur.com/WmlUzED.png'
        ],
        dab: [
            'https://i.imgur.com/BbSQqqH.png',
            'https://i.imgur.com/wxWpOkN.jpg',
            'https://i.imgur.com/neOJFG7.jpg',
            'https://i.imgur.com/xfStDTj.png',
            'https://i.imgur.com/7MOQKq9.png',
            'https://i.imgur.com/V9OUNQ3.png',
            'https://i.imgur.com/O685qOR.jpg',
            'https://i.imgur.com/yJSmD6m.png',
            'https://i.imgur.com/KLsbQ8S.png',
            'https://i.imgur.com/9uZb8EQ.jpg',
            'https://i.imgur.com/Ndgo1BO.png'
        ],
        disgust: [
            'https://i.imgur.com/1UBtEAB.jpg',
            'https://i.imgur.com/h97YqqW.jpg',
            'https://i.imgur.com/VKoEpAl.jpg',
            'https://i.imgur.com/bCHvbne.jpg',
            'https://i.imgur.com/bejXt7W.jpg'
        ]
    }
}
