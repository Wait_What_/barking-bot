# Help
## Currency
### Balance
- See your balance
- Aliases: `bal`, `$`
- Usage: `balance amount`
### Drop
- Drop Barkcoin in a channel (5% tax)
- Usage: `drop amount`
### Frequency
- Set currency generation frequency
- Usage: `frequency 15-100`
- You need `MANAGE MESSAGES` to run this command
### Generate
- Toggle currency generation in a channel
- Usage: `generate [#channel]`
- You need `MANAGE MESSAGES` to run this command
### Give
- Give Barkcoin to a member
- Aliases: `gift`
- Usage: `give @member amount`
### Leaderboard
- See the top 15 users
- Aliases: `top`, `lb`
### Roleshop
- Use the role shop
- Aliases: `shop`, `store`, `rolestore`
- Usage: `roleshop [buy/sell/add/remove role-name [price]]`
## Image
### Cat
- Get a cat image
- Aliases: `meow`, `nyaa`
### Dog
- Get a dog image
- Aliases: `woof`, `bark`
### Angry
- Get an angry image
### Dab
- Get a dab image
### Disgust
- Get a disgusted image
- Aliases: `disgusting`, `disgusted`
### Hug
- Hug someone
- Aliases: `cuddle`
- Usage: `hug @member`
### Kiss
- Kiss someone
- Usage: `kiss @member`
### Slap
- Slap someone
- Usage: `slap @member`
### Smug
- Get a smug image
## Misc
### Avatar
- See a member's avatar
- Aliases: `pfp`, `picture`, `profile`, `av`
- Usage: `avatar [@member]`
### Color
- Get a (random) color
- Aliases: `colour`
- Usage: `color [hex-color]`
### Eula
- See the bot's EULA
### Greetoptions
- Change greet options
- Aliases: `joinoptions`, `leaveoptions`
- Usage: `greetoptions [embed] [dm]`
- You need `MANAGE MESSAGES` to run this command
### Help
- See the list of all commands
### Info
- See info about the bot
- Aliases: `stats`, `information`, `invite`, `bugs`, `support`
### Joinmessage
- Change the leave message in this channel
- Aliases: `greet`, `greeting`
- Usage: `joinmessage [text]`
- You need `MANAGE MESSAGES` to run this command
### Leavemessage
- Change the leave message in this channel
- Aliases: `goodbye`
- Usage: `leavemessage [text]`
- You need `MANAGE MESSAGES` to run this command
### Memberinfo
- See info about a member
- Aliases: `whois`, `userinfo`
### Ping
- See the bot's ping
- Aliases: `pong`
### Poll
- Create a poll
- Aliases: `p`
- Usage: `poll question [| option-a | option-b | ...]`
### Prefix
- Change the prefix in this server
- Aliases: `setprefix`
- Usage: `prefix [new-prefix]`
- You need `MANAGE MESSAGES` to run this command
### Generateqr
- Generate a QR code
- Aliases: `qr`, `qrcode`, `makeqr`, `genqr`, `createqr`
- Usage: `generateqr text`
### Serverinfo
- See info about this server
- Aliases: `guildinfo`
## Mod
### Ban
- Ban a member
- Aliases: `b`
- Usage: `ban @member [reason]`
- You need `BAN MEMBERS` to run this command
### Clear
- Bulk delete messages
- Aliases: `purge`, `clean`, `delete`, `c`
- Usage: `clear 1-99 / @member`
- You need `MANAGE MESSAGES` to run this command
### Clearwarnings
- Clear a member's warnings
- Aliases: `clearwarning`, `cw`
- Usage: `clearwarnings @member`
- You need `KICK MEMBERS` to run this command
### Hackban
- Ban a member before they join
- Aliases: `hb`
- Usage: `hackban user-id [reason]`
- You need `BAN MEMBERS` to run this command
### Kick
- Kick a member
- Aliases: `k`
- Usage: `kick @member [reason]`
- You need `KICK MEMBERS` to run this command
### Rename
- Change a member's nickname
- Aliases: `nick`, `nickname`
- Usage: `rename @member [name]`
- You need `MANAGE NICKNAMES` to run this command
### Role
- Add/remove a role
- Usage: `role @member role`
- You need `MANAGE ROLES` to run this command
### Softban
- Ban and instantly unban a member
- Aliases: `sb`
- Usage: `softban @member [reason]`
- You need `BAN MEMBERS` to run this command
### Unban
- Unban a banned user
- Aliases: `ub`
- Usage: `unban user-id [reason]`
- You need `BAN MEMBERS` to run this command
### Undelete
- Restore the last deleted message
- Aliases: `snipe`
- Usage: `undelete [@member]`
- You need `MANAGE MESSAGES` to run this command
### Warn
- Warn a member
- Aliases: `w`
- Usage: `warn @member reason`
- You need `KICK MEMBERS` to run this command
### Warnings
- See a member's warnings
- Aliases: `warns`
- Usage: `warnings @member`
- You need `KICK MEMBERS` to run this command
## Search
### Anime
- Search for anime on MyAnimeList
- Aliases: `mal`
- Usage: `anime anime-name`
### E621
- Search for images on e621
- Aliases: `e6`, `furry`
- Usage: `e621 tags`
### Manga
- Search for manga on MyAnimeList
- Usage: `manga manga-name`
### Safebooru
- Search for images on Safebooru
- Aliases: `booru`
- Usage: `safebooru tags`
### Urban
- Search on Urban Dictionary
- Aliases: `ud`, `define`
- Usage: `urban request`
### Youtube
- Search on YouTube
- Aliases: `yt`
- Usage: `youtube request`